homebrew
=========

Wrapper role for homebrew.

Requirements
------------

Homebrew (https://brew.sh/) must be installed.

Role Variables
--------------

| Variable                  | Required | Description                                         | Default |
|---------------------------|----------|-----------------------------------------------------|---------|
| homebrew__packages_present | false    | List of package names to install via homebrew.      | []      |
| homebrew__packages_absent  | false    | List of package names *not* installed via homebrew. | []      |

Dependencies
------------

```yaml
collections:
  - name: community.general
    version: 7.1.0
```

Example Playbook
----------------
Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/homebrew.git
    version: stable
    scm: git
    name: ibox.homebrew
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.homebrew"
      ansible.builtin.include_role:
        name: "ibox.home_files"
      vars:
        homebrew__packages_present:
          - htop
          - bash
        homebrew__packages_absent: ['ansible']

```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
